<?php

namespace Module\C360\GwAddonRisorse\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="gw_modelli")
 */
class Modello
{
	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=50)
	 */
	private $codice;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $descrizione;

	/**
	 * @ORM\Column(type="string", length=100, nullable=true)
	 */
	private $stagione;

	/**
	 * @ORM\ManyToOne(targetEntity="Taglia", inversedBy="modelli")
	 * @ORM\JoinColumn(name="taglia_id", referencedColumnName="id", nullable=true)
	 */
	private $taglia;

	/**
	 * @ORM\ManyToOne(targetEntity="Tessuto", inversedBy="modelli")
	 * @ORM\JoinColumn(name="tessuto_id", referencedColumnName="id", nullable=true)
	 */
	private $tessuto;

	/**
	 * @ORM\ManyToOne(targetEntity="Accessorio", inversedBy="modelli")
	 * @ORM\JoinColumn(name="accessorio_id", referencedColumnName="id", nullable=true)
	 */
	private $accessorio;

	/**
	 * @ORM\ManyToOne(targetEntity="Colore", inversedBy="modelli")
	 * @ORM\JoinColumn(name="colore_id", referencedColumnName="id", nullable=true)
	 */
	private $colore;

	/**
	 * @ORM\Column(type="decimal", precision=8, scale=2)
	 */
	private $quantita;

	/**
	 * @ORM\Column(type="decimal", precision=8, scale=2)
	 */
	private $prezzo_vendita;

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	public function __construct()
	{
		$this->prezzo_vendita = 0;
		$this->quantita = 0;
	}

	public function __toString()
	{
		return $this->getDescrizione();
	}

    /**
     * Set codice
     *
     * @param string $codice
     *
     * @return Modello
     */
    public function setCodice($codice)
    {
        $this->codice = $codice;

        return $this;
    }

    /**
     * Get codice
     *
     * @return string
     */
    public function getCodice()
    {
        return $this->codice;
    }

    /**
     * Set descrizione
     *
     * @param string $descrizione
     *
     * @return Modello
     */
    public function setDescrizione($descrizione)
    {
        $this->descrizione = $descrizione;

        return $this;
    }

    /**
     * Get descrizione
     *
     * @return string
     */
    public function getDescrizione()
    {
        return $this->descrizione;
    }

    /**
     * Set stagione
     *
     * @param string $stagione
     *
     * @return Modello
     */
    public function setStagione($stagione)
    {
        $this->stagione = $stagione;

        return $this;
    }

    /**
     * Get stagione
     *
     * @return string
     */
    public function getStagione()
    {
        return $this->stagione;
    }

    /**
     * Set quantita
     *
     * @param string $quantita
     *
     * @return Modello
     */
    public function setQuantita($quantita)
    {
        $this->quantita = $quantita;

        return $this;
    }

    /**
     * Get quantita
     *
     * @return string
     */
    public function getQuantita()
    {
        return $this->quantita;
    }

    /**
     * Set prezzoVendita
     *
     * @param string $prezzoVendita
     *
     * @return Modello
     */
    public function setPrezzoVendita($prezzoVendita)
    {
        $this->prezzo_vendita = $prezzoVendita;

        return $this;
    }

    /**
     * Get prezzoVendita
     *
     * @return string
     */
    public function getPrezzoVendita()
    {
        return $this->prezzo_vendita;
    }

    /**
     * Set taglia
     *
     * @param \Module\C360\GwAddonRisorse\Entity\Taglia $taglia
     *
     * @return Modello
     */
    public function setTaglia(\Module\C360\GwAddonRisorse\Entity\Taglia $taglia = null)
    {
        $this->taglia = $taglia;

        return $this;
    }

    /**
     * Get taglia
     *
     * @return \Module\C360\GwAddonRisorse\Entity\Taglia
     */
    public function getTaglia()
    {
        return $this->taglia;
    }

    /**
     * Set tessuto
     *
     * @param \Module\C360\GwAddonRisorse\Entity\Tessuto $tessuto
     *
     * @return Modello
     */
    public function setTessuto(\Module\C360\GwAddonRisorse\Entity\Tessuto $tessuto = null)
    {
        $this->tessuto = $tessuto;

        return $this;
    }

    /**
     * Get tessuto
     *
     * @return \Module\C360\GwAddonRisorse\Entity\Tessuto
     */
    public function getTessuto()
    {
        return $this->tessuto;
    }

    /**
     * Set accessorio
     *
     * @param \Module\C360\GwAddonRisorse\Entity\Accessorio $accessorio
     *
     * @return Modello
     */
    public function setAccessorio(\Module\C360\GwAddonRisorse\Entity\Accessorio $accessorio = null)
    {
        $this->accessorio = $accessorio;

        return $this;
    }

    /**
     * Get accessorio
     *
     * @return \Module\C360\GwAddonRisorse\Entity\Accessorio
     */
    public function getAccessorio()
    {
        return $this->accessorio;
    }

    /**
     * Set colore
     *
     * @param \Module\C360\GwAddonRisorse\Entity\Colore $colore
     *
     * @return Modello
     */
    public function setColore(\Module\C360\GwAddonRisorse\Entity\Colore $colore = null)
    {
        $this->colore = $colore;

        return $this;
    }

    /**
     * Get colore
     *
     * @return \Module\C360\GwAddonRisorse\Entity\Colore
     */
    public function getColore()
    {
        return $this->colore;
    }
}
