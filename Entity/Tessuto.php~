<?php

namespace Module\C360\GwAddonRisorse\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="gw_tessuti")
 */
class Tessuto
{
	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=50, unique=true)
	 */
	private $codice;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $descrizione;

	/**
	 * @ORM\Column(type="decimal", precision=8, scale=2)
	 */
	private $quantita;

	/**
	 * @ORM\Column(type="decimal", precision=8, scale=2)
	 */
	private $prezzo_acquisto;

	/**
	 * @ORM\ManyToMany(targetEntity="\Module\C360\GwBase\Entity\Fornitore")
	 * @ORM\JoinTable(name="gw_fornitori_tessuti",
	 *      joinColumns={@ORM\JoinColumn(name="tessuto_id", referencedColumnName="id", unique=true)},
	 *      inverseJoinColumns={@ORM\JoinColumn(name="fornitore_id", referencedColumnName="id")}
	 *      )
	 */
	private $fornitori;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fornitori = new ArrayCollection();
    }

	public function __toString()
	{
		return $this->getDescrizione();
	}


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set codice
     *
     * @param string $codice
     *
     * @return Tessuto
     */
    public function setCodice($codice)
    {
        $this->codice = $codice;

        return $this;
    }

    /**
     * Get codice
     *
     * @return string
     */
    public function getCodice()
    {
        return $this->codice;
    }

    /**
     * Set descrizione
     *
     * @param string $descrizione
     *
     * @return Tessuto
     */
    public function setDescrizione($descrizione)
    {
        $this->descrizione = $descrizione;

        return $this;
    }

    /**
     * Get descrizione
     *
     * @return string
     */
    public function getDescrizione()
    {
        return $this->descrizione;
    }

    /**
     * Set quantita
     *
     * @param string $quantita
     *
     * @return Tessuto
     */
    public function setQuantita($quantita)
    {
        $this->quantita = $quantita;

        return $this;
    }

    /**
     * Get quantita
     *
     * @return string
     */
    public function getQuantita()
    {
        return $this->quantita;
    }

    /**
     * Set prezzoAcquisto
     *
     * @param string $prezzoAcquisto
     *
     * @return Tessuto
     */
    public function setPrezzoAcquisto($prezzoAcquisto)
    {
        $this->prezzo_acquisto = $prezzoAcquisto;

        return $this;
    }

    /**
     * Get prezzoAcquisto
     *
     * @return string
     */
    public function getPrezzoAcquisto()
    {
        return $this->prezzo_acquisto;
    }

    /**
     * Add fornitori
     *
     * @param \Module\C360\GwBase\Entity\Fornitore $fornitori
     *
     * @return Tessuto
     */
    public function addFornitori(\Module\C360\GwBase\Entity\Fornitore $fornitori)
    {
        $this->fornitori[] = $fornitori;

        return $this;
    }

    /**
     * Remove fornitori
     *
     * @param \Module\C360\GwBase\Entity\Fornitore $fornitori
     */
    public function removeFornitori(\Module\C360\GwBase\Entity\Fornitore $fornitori)
    {
        $this->fornitori->removeElement($fornitori);
    }

    /**
     * Get fornitori
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFornitori()
    {
        return $this->fornitori;
    }
}
