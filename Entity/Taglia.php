<?php

namespace Module\C360\GwAddonRisorse\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="gw_taglie")
 */
class Taglia
{
	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\ManyToOne(targetEntity="GruppoTaglie", inversedBy="taglie")
	 */
	private $gruppo_taglie;

	/**
	 * @ORM\Column(type="string", length=50)
	 */
	private $codice;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $descrizione;

	public function __construct()
	{
		$this->taglie = new ArrayCollection();
	}

	/**
	 * Get id
	 *
	 * @return integer
	 */
	public function getId()
	{
		return $this->id;
	}

	public function __toString()
	{
		return sprintf(
			'%s (%s)',
			$this->getCodice(),
			(($this->getGruppoTaglie() !== null) ? $this->getGruppoTaglie()->__toString() : '')
		);
	}

	/**
	 * @return GruppoTaglie
	 */
	public function getGruppoTaglie()
	{
		return $this->gruppo_taglie;
	}

	/**
	 * @param GruppoTaglie $gruppo_taglie
	 * @return $this
	 */
	public function setGruppoTaglie(GruppoTaglie $gruppo_taglie)
	{
		$this->gruppo_taglie = $gruppo_taglie;

		return $this;
	}

	/**
	 * Set codice
	 *
	 * @param string $codice
	 *
	 * @return GruppoTaglie
	 */
	public function setCodice($codice)
	{
		$this->codice = $codice;

		return $this;
	}

	/**
	 * Get codice
	 *
	 * @return string
	 */
	public function getCodice()
	{
		return $this->codice;
	}

	/**
	 * Set descrizione
	 *
	 * @param string $descrizione
	 *
	 * @return GruppoTaglie
	 */
	public function setDescrizione($descrizione)
	{
		$this->descrizione = $descrizione;

		return $this;
	}

	/**
	 * Get descrizione
	 *
	 * @return string
	 */
	public function getDescrizione()
	{
		return $this->descrizione;
	}
}
