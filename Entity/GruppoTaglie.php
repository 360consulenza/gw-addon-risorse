<?php

namespace Module\C360\GwAddonRisorse\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="gw_gruppi_taglie")
 */
class GruppoTaglie
{
	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=50)
	 */
	private $codice;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $descrizione;

	/**
	 * @ORM\OneToMany(targetEntity="Taglia", mappedBy="gruppo_taglie", cascade={"remove"})
	 */
	private $taglie;

	public function __construct()
	{
		$this->taglie = new ArrayCollection();
	}

	/**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

	public function __toString()
	{
		return $this->getDescrizione();
	}

    /**
     * Set codice
     *
     * @param string $codice
     *
     * @return GruppoTaglie
     */
    public function setCodice($codice)
    {
        $this->codice = $codice;

        return $this;
    }

    /**
     * Get codice
     *
     * @return string
     */
    public function getCodice()
    {
        return $this->codice;
    }

    /**
     * Set descrizione
     *
     * @param string $descrizione
     *
     * @return GruppoTaglie
     */
    public function setDescrizione($descrizione)
    {
        $this->descrizione = $descrizione;

        return $this;
    }

    /**
     * Get descrizione
     *
     * @return string
     */
    public function getDescrizione()
    {
        return $this->descrizione;
    }

	/**
	 * @param Taglia $taglia
	 * @return $this
	 */
	public function addTaglia(Taglia $taglia)
	{
		$this->taglie[] = $taglia;

		return $this;
	}

	/**
	 * @param Taglia $taglia
	 */
	public function removeTaglia(Taglia $taglia)
	{
		$this->taglie->removeElement($taglia);
	}

	/**
	 * @return Taglia[]
	 */
	public function getTaglie()
	{
		return $this->taglie;
	}

    /**
     * Add taglie
     *
     * @param \Module\C360\GwAddonRisorse\Entity\Taglia $taglie
     *
     * @return GruppoTaglie
     */
    public function addTaglie(\Module\C360\GwAddonRisorse\Entity\Taglia $taglie)
    {
        $this->taglie[] = $taglie;

        return $this;
    }

    /**
     * Remove taglie
     *
     * @param \Module\C360\GwAddonRisorse\Entity\Taglia $taglie
     */
    public function removeTaglie(\Module\C360\GwAddonRisorse\Entity\Taglia $taglie)
    {
        $this->taglie->removeElement($taglie);
    }
}
