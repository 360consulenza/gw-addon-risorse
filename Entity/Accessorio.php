<?php

namespace Module\C360\GwAddonRisorse\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="gw_accessori")
 */
class Accessorio
{
	/**
	 * @ORM\Column(type="integer")
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
	 * @ORM\OneToMany(targetEntity="Modello", mappedBy="accessorio")
	 */
	private $articoli;

	/**
	 * @ORM\Column(type="string", length=50)
	 */
	private $codice;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $descrizione;

	/**
	 * @ORM\Column(type="decimal", precision=8, scale=2)
	 */
	private $quantita;

	/**
	 * @ORM\Column(type="decimal", precision=8, scale=2)
	 */
	private $prezzo_acquisto;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

	public function __construct()
	{
		$this->prezzo_acquisto = 0;
		$this->giacenza = 0;
	}

	public function __toString()
	{
		return $this->codice;
	}

    /**
     * Set codice
     *
     * @param string $codice
     *
     * @return Accessorio
     */
    public function setCodice($codice)
    {
        $this->codice = $codice;

        return $this;
    }

    /**
     * Get codice
     *
     * @return string
     */
    public function getCodice()
    {
        return $this->codice;
    }

    /**
     * Set descrizione
     *
     * @param string $descrizione
     *
     * @return Accessorio
     */
    public function setDescrizione($descrizione)
    {
        $this->descrizione = $descrizione;

        return $this;
    }

    /**
     * Get descrizione
     *
     * @return string
     */
    public function getDescrizione()
    {
        return $this->descrizione;
    }

    /**
     * Set quantita
     *
     * @param string $quantita
     *
     * @return Accessorio
     */
    public function setQuantita($quantita)
    {
        $this->quantita = $quantita;

        return $this;
    }

    /**
     * Get quantita
     *
     * @return string
     */
    public function getQuantita()
    {
        return $this->quantita;
    }

    /**
     * Set prezzo_acquisto
     *
     * @param string $prezzo
     *
     * @return Accessorio
     */
    public function setPrezzoAcquisto($prezzo_acquisto)
    {
        $this->prezzo_acquisto = $prezzo_acquisto;

        return $this;
    }

    /**
     * Get prezzo_acquisto
     *
     * @return string
     */
    public function getPrezzoAcquisto()
    {
        return $this->prezzo_acquisto;
    }

    /**
     * Add articoli
     *
     * @param \Module\C360\GwAddonRisorse\Entity\Modello $articoli
     *
     * @return Accessorio
     */
    public function addArticoli(\Module\C360\GwAddonRisorse\Entity\Modello $articoli)
    {
        $this->articoli[] = $articoli;

        return $this;
    }

    /**
     * Remove articoli
     *
     * @param \Module\C360\GwAddonRisorse\Entity\Modello $articoli
     */
    public function removeArticoli(\Module\C360\GwAddonRisorse\Entity\Modello $articoli)
    {
        $this->articoli->removeElement($articoli);
    }

    /**
     * Get articoli
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getArticoli()
    {
        return $this->articoli;
    }
}
