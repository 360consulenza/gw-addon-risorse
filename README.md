# Documentale

Modulo **GwAddonRisorse** per **360C Symfony Framework**.

## Descrizione

Addon per GwBase con alcune personalizzazioni per **Risorse**.

## Installazione

Eseguire il comando:

    php app/console 360c:module:install gw-addon-risorse
