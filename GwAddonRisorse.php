<?php

namespace Module\C360\GwAddonRisorse;

use GestionaleBundle\Entity\Permesso;
use GestionaleBundle\Module;

class GwAddonRisorse extends Module
{
	public static $VERSION = '1.0.0';

	public function getName()
	{
		return 'GwAddonRisorse';
	}

	public function getVersion()
	{
		return GwAddonRisorse::$VERSION;
	}

	public function getPermissions()
	{
		return array(
			new Permesso(GwAddonRisorse::class, null, 'access', 'Acesso al modulo Risorse'),
			new Permesso(GwAddonRisorse::class, null, 'gest_modelli', 'Gestione Modelli'),
			new Permesso(GwAddonRisorse::class, null, 'gest_tessuti', 'Gestione Tessuti'),
			new Permesso(GwAddonRisorse::class, null, 'gest_accessori', 'Gestione Accessori'),
			new Permesso(GwAddonRisorse::class, null, 'nuovo_accessorio', 'Creare un nuovo accessorio'),
			new Permesso(GwAddonRisorse::class, null, 'modifica_accessorio', 'Modifica un accessorio'),
			new Permesso(GwAddonRisorse::class, null, 'elimina_accessorio', 'Elimina un accessorio'),
			new Permesso(GwAddonRisorse::class, null, 'gest_taglie', 'Gestione Taglie'),
			new Permesso(GwAddonRisorse::class, null, 'stampa', 'Stampa'),
			new Permesso(GwAddonRisorse::class, null, 'gest_colori', 'Gestione Colori'),
		);
	}

	public function getMenu()
	{
		return array(
			'Abbigliamento'		=> array(
				'Modelli'					=> array('gw_modelli_index', 'gest_modelli', 'female', 'abbigliamento'),
				'Tessuti'					=> array('gw_tessuti_index', 'gest_tessuti', 'barcode', 'abbigliamento'),
				'Accessori'					=> array('gw_accessori_index', 'gest_accessori', 'times-circle-o', 'abbigliamento'),
				'Taglie'					=> array('gw_taglie_index', 'gest_taglie', 'sort-amount-asc', 'abbigliamento'),
				'Gruppi Taglie'				=> array('gw_gruppi_taglie_index', 'gest_taglie', 'list', 'abbigliamento'),
				'Stampa Inventario'			=> array('gw_stampa_inventario_index', 'stampa', 'print', 'abbigliamento'),
				'Stampa Articoli'			=> array('gw_stampa_articoli_index', 'stampa', 'print', 'abbigliamento'),
				'Colori'					=> array('gw_colori_index', 'gest_colori', 'paint-brush', 'abbigliamento'),
			),
		);
	}
}
