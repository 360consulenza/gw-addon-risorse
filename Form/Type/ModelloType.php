<?php

namespace Module\C360\GwAddonRisorse\Form\Type;

use Module\C360\GwBase\Licenza;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ModelloType extends AbstractType
{
	const READ_ONLY = 0;
	const CREATE = 1;
	const EDIT = 2;

	/**
	 * @var Licenza
	 */
	private $license;

	/**
	 * @param Licenza $license
	 */
	public function __construct(Licenza $license)
	{
		$this->license = $license;
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder

			->add('codice', TextType::class, array(
				'label'			=> 'Codice',
				'required'		=> true,
				'attr'			=> array(
					'autofocus'	=> true,
				),
			))

			->add('descrizione', TextareaType::class, array(
				'label'			=> 'Descrizione',
				'required'		=> false,
			))

			->add('stagione', ChoiceType::class, array(
				'label'			=> 'Stagione',
				'required'		=> true,
				'choices' 		=> array('ai' => 'Autunno/Inverno', 'pe' => 'Primavera/Estate'),
			))

			->add('taglia', EntityType::class, array(
					'class'		=> 'GwAddonRisorseBundle:Taglia',
					'placeholder'	=> '',
					'label'		=> 'Taglia',
					'required'	=> true,
				))

			->add('tessuto', EntityType::class, array(
				'class'		=> 'GwAddonRisorseBundle:Tessuto',
				'placeholder'	=> '',
				'label'			=> 'Tessuto',
				'required'		=> false,
			))

			->add('accessorio', EntityType::class, array(
				'class'		=> 'GwAddonRisorseBundle:Accessorio',
				'placeholder'	=> '',
				'label'			=> 'Accessori',
				'required'		=> true,
			))

			->add('colore', EntityType::class, array(
				'class'		=> 'GwAddonRisorseBundle:Colore',
				'placeholder'	=> '',
				'label'			=> 'Colore',
				'required'		=> true,
			))

			->add('prezzo_vendita', NumberType::class, array(
				'label'			=> 'Prezzo',
				'required'		=> true,
			))

			->add('quantita', NumberType::class, array(
				'label'			=> 'Quantità',
				'required'		=> true,
			))

			;

	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Module\C360\GwAddonRisorse\Entity\Modello',
		));
	}
}
