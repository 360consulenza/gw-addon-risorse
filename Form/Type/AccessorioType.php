<?php

namespace Module\C360\GwAddonRisorse\Form\Type;

use Module\C360\GwBase\Licenza;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AccessorioType extends AbstractType
{
	const READ_ONLY = 0;
	const CREATE = 1;
	const EDIT = 2;

	/**
	 * @var Licenza
	 */
	private $license;

	/**
	 * @param Licenza $license
	 */
	public function __construct(Licenza $license)
	{
		$this->license = $license;
	}

	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder

			->add('codice', TextType::class, array(
				'label'			=> 'Codice',
				'required'		=> true,
				'attr'			=> array(
					'autofocus'	=> true,
				),
			))

			->add('descrizione', TextareaType::class, array(
				'label'			=> 'Descrizione',
				'required'		=> false,
			))

			->add('quantita', NumberType::class, array(
				'label'			=> 'Quantità',
				'required'		=> true,
			))

			->add('prezzo_acquisto', NumberType::class, array(
					'label'			=> 'Prezzo Acquisto',
					'required'		=> true,
				))

			;

	}

	public function configureOptions(OptionsResolver $resolver)
	{
		$resolver->setDefaults(array(
			'data_class' => 'Module\C360\GwAddonRisorse\Entity\Accessorio',
		));
	}
}
