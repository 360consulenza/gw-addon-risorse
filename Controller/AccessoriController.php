<?php

namespace Module\C360\GwAddonRisorse\Controller;

use GestionaleBundle\GestionaleController;
use Module\C360\GwAddonRisorse\Entity\Accessorio;
use Module\C360\GwAddonRisorse\Form\Type\AccessorioType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AccessoriController extends GestionaleController
{
	public function indexAction()
	{
		$this->requiredPermission('access');

		$licenza = $this->get('c360.gwbase.licenza');

		if ($licenza->has('stagione_modelli'))
		{
			// fai questo
		}

		return $this->render('GwAddonRisorseBundle:Accessori:index.html.twig');
	}

	public function nuovoAction(Request $request)
	{
		$this->requiredPermission('nuovo_accessorio');

		$accessorio = new Accessorio();

		return $this->form($request, $accessorio, AccessorioType::CREATE);
	}

	public function modificaAction($id, Request $request)
	{
		$this->requiredPermission('modifica_accessorio');

		$accessorio = $this
			->getDoctrine()
			->getRepository('GwAddonRisorseBundle:Accessorio')
			->findOneBy(array(
				'id'	=> $id,
			));

		if (!$accessorio)
		{
			return $this->redirectToRoute('gw_accessori_index');
		}

		return $this->form($request, $accessorio, AccessorioType::EDIT);
	}

	public function eliminaAction($id)
	{
		$this->requiredPermission('elimina_accessorio');

		$accessorio = $this
			->getDoctrine()
			->getRepository('GwAddonRisorseBundle:Accessorio')
			->findOneBy(array(
				'id'	=> $id,
			));

		if ($accessorio instanceof Accessorio)
		{
			$manager = $this
				->getDoctrine()
				->getManager();

			$this->logger->logUserAction(sprintf("ha eliminato il accessorio %s", $accessorio->__toString()));

			$manager->remove($accessorio);
			$manager->flush();
		}

		return $this->redirectToRoute('gw_accessori_index');
	}

	public function listaAction()
	{
		$this->requiredPermission('gest_accessori');

		$accessori = $this
			->getDoctrine()
			->getRepository('GwAddonRisorseBundle:Accessorio')
			->findBy(array(), array(
				'descrizione'	=> 'ASC',
			));

		$_accessori = array();

		foreach($accessori as $accessorio)
		{
			$_accessori[] = array(
				'id'				=> $accessorio->getId(),
				'codice'			=> $accessorio->getCodice(),
				'descrizione'		=> $accessorio->getDescrizione(),
				'giacenza'			=> $accessorio->getQuantita(),
				'prezzo_acquisto'	=> number_format($accessorio->getPrezzoAcquisto(), 2, ',', '.'),
			);
		}

		$response = new Response();
		$response->setContent(json_encode($_accessori));
		$response->headers->set('Content-Type', 'application/json');

		return $response;
	}

	private function form(Request $request, Accessorio $accessorio, $type)
	{
		$form = $this->createForm(new AccessorioType($this->get('c360.gwbase.licenza')), $accessorio);
		$form->handleRequest($request);

		if ($form->isValid())
		{
			$db = $this->getDoctrine();
			$manager = $db->getManager();

			if ($type == AccessorioType::CREATE)
			{
				$this->logger->logUserAction(sprintf("ha creato il accessorio %s", $accessorio->__toString()));
			}
			else
			{
				$this->logger->logUserAction(sprintf("ha modificato il accessorio %s", $accessorio->__toString()));
			}

			$manager->persist($accessorio);
			$manager->flush();

			return $this->redirectToRoute('gw_accessori_index');
		}

		return $this->render('GwAddonRisorseBundle:Accessori:form.html.twig', array(
			'form'			=> $form->createView(),
		));
	}

}
