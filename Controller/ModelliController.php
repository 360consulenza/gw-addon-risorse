<?php

namespace Module\C360\GwAddonRisorse\Controller;

use GestionaleBundle\GestionaleController;
use Module\C360\GwAddonRisorse\Entity\Modello;
use Module\C360\GwAddonRisorse\Form\Type\ModelloType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ModelliController extends GestionaleController
{
	public function indexAction()
	{
		$this->requiredPermission('access');

		return $this->render('GwAddonRisorseBundle:Modelli:index.html.twig');
	}

	public function nuovoAction(Request $request)
	{
		$this->requiredPermission('nuovo_modello');

		$modello = new Modello();

		return $this->form($request, $modello, ModelloType::CREATE);
	}

	public function modificaAction($id, Request $request)
	{
		$this->requiredPermission('modifica_modello');

		$modello = $this
			->getDoctrine()
			->getRepository('GwAddonRisorseBundle:Modello')
			->findOneBy(array(
				'id'	=> $id,
			));

		if (!$modello)
		{
			return $this->redirectToRoute('gw_modelli_index');
		}

		return $this->form($request, $modello, ModelloType::EDIT);
	}

	public function eliminaAction($id)
	{
		$this->requiredPermission('elimina_modello');

		$modello = $this
			->getDoctrine()
			->getRepository('GwAddonRisorseBundle:Modello')
			->findOneBy(array(
				'id'	=> $id,
			));

		if ($modello instanceof Modello)
		{
			$manager = $this
				->getDoctrine()
				->getManager();

			$this->logger->logUserAction(sprintf("ha eliminato l'modello %s", $modello->__toString()));

			$manager->remove($modello);
			$manager->flush();
		}

		return $this->redirectToRoute('gw_modelli_index');
	}

	public function listaAction()
	{
		$this->requiredPermission('gest_modelli');

		$modelli = $this
			->getDoctrine()
			->getRepository('GwAddonRisorseBundle:Modello')
			->findBy(array(), array(
				'descrizione'	=> 'ASC',
			));

		$_modelli = array();

		foreach($modelli as $modello)
		{
			$_modelli[] = array(
				'id'				=> $modello->getId(),
				'codice'			=> $modello->getCodice(),
				'descrizione'		=> $modello->getDescrizione(),
				'stagione'			=> $modello->getStagione(),
				'quantita'			=> $modello->getQuantita(),
				'prezzo_vendita'	=> number_format($modello->getPrezzoVendita(), 2, ',', '.'),
			);
		}

		$response = new Response();
		$response->setContent(json_encode($_modelli));
		$response->headers->set('Content-Type', 'application/json');

		return $response;
	}

	private function form(Request $request, Modello $modello, $type)
	{
		$form = $this->createForm(new ModelloType($this->get('c360.gwbase.licenza')), $modello);
		$form->handleRequest($request);

		if ($form->isValid())
		{
			$db = $this->getDoctrine();
			$manager = $db->getManager();

			if ($type == ModelloType::CREATE)
			{
				$this->logger->logUserAction(sprintf("ha creato il modello %s", $modello->__toString()));
			}
			else
			{
				$this->logger->logUserAction(sprintf("ha modificato il modello %s", $modello->__toString()));
			}

			$manager->persist($modello);
			$manager->flush();

			return $this->redirectToRoute('gw_modelli_index');
		}

		return $this->render('GwAddonRisorseBundle:Modelli:form.html.twig', array(
			'form'			=> $form->createView(),
		));
	}
}
