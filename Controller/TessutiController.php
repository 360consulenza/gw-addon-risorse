<?php

namespace Module\C360\GwAddonRisorse\Controller;

use GestionaleBundle\GestionaleController;
use Module\C360\GwAddonRisorse\Entity\Tessuto;
use Module\C360\GwAddonRisorse\Entity\FornitoreTessuto;
use	Module\C360\GwAddonRisorse\Form\Type\TessutoType;
use Module\C360\GwBase\Entity\Fornitore;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TessutiController extends GestionaleController
{
	public function indexAction()
	{
		$this->requiredPermission('access');

		return $this->render('GwAddonRisorseBundle:Tessuti:index.html.twig');
	}

	public function nuovoAction(Request $request)
	{
		$this->requiredPermission('nuovo_tessuto');

		$tessuto = new Tessuto();

		return $this->form($request, $tessuto, TessutoType::CREATE);
	}

	public function modificaAction($id, Request $request)
	{
		$this->requiredPermission('modifica_tessuto');

		$tessuto = $this
			->getDoctrine()
			->getRepository('GwAddonRisorseBundle:Tessuto')
			->findOneBy(array(
				'id'	=> $id,
			));

		if (!$tessuto)
		{
			return $this->redirectToRoute('gw_tessuti_index');
		}

		return $this->form($request, $tessuto, TessutoType::EDIT);
	}

	public function eliminaAction($id)
	{
		$this->requiredPermission('elimina_tessuto');

		$tessuto = $this
			->getDoctrine()
			->getRepository('GwAddonRisorseBundle:Tessuto')
			->findOneBy(array(
				'id'	=> $id,
			));

		if ($tessuto instanceof Tessuto)
		{
			$manager = $this
				->getDoctrine()
				->getManager();

			$this->logger->logUserAction(sprintf("ha eliminato il tessuto %s", $tessuto->__toString()));

			$manager->remove($tessuto);
			$manager->flush();
		}

		return $this->redirectToRoute('gw_tessuti_index');
	}

	public function listaAction()
	{
		$this->requiredPermission('gest_tessuti');

		$tessuti = $this
			->getDoctrine()
			->getRepository('GwAddonRisorseBundle:Tessuto')
			->findBy(array(), array(
				'descrizione'	=> 'ASC',
			));

		$_tessuti = array();

		foreach($tessuti as $tessuto)
		{
			$_tessuti[] = array(
				'id'				=> $tessuto->getId(),
				'codice'			=> $tessuto->getCodice(),
				'descrizione'		=> $tessuto->getDescrizione(),
				'quantita'			=> $tessuto->getQuantita(),
				'prezzo_acquisto'	=> number_format($tessuto->getPrezzoAcquisto(), 2, ',', '.'),
			);
		}

		$response = new Response();
		$response->setContent(json_encode($_tessuti));
		$response->headers->set('Content-Type', 'application/json');

		return $response;
	}

	private function form(Request $request, Tessuto $tessuto, $type)
	{
		$form = $this->createForm(new TessutoType($this->get('c360.gwbase.licenza')), $tessuto);
		$form->handleRequest($request);

		if ($form->isValid())
		{
			$db = $this->getDoctrine();
			$manager = $db->getManager();

			if ($type == TessutoType::CREATE)
			{
				$this->logger->logUserAction(sprintf("ha creato il tessuto %s", $tessuto->__toString()));
			}
			else
			{
				$this->logger->logUserAction(sprintf("ha modificato il tessuto %s", $tessuto->__toString()));
			}

			$manager->persist($tessuto);
			$manager->flush();

			return $this->redirectToRoute('gw_tessuti_index');
		}

		return $this->render('GwAddonRisorseBundle:Tessuti:form.html.twig', array(
			'form'			=> $form->createView(),
		));
	}
}
