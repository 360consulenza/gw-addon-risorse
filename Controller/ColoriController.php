<?php

namespace Module\C360\GwAddonRisorse\Controller;

use GestionaleBundle\GestionaleController;
use Module\C360\GwAddonRisorse\Entity\Colore;
use	Module\C360\GwAddonRisorse\Form\Type\ColoreType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ColoriController extends GestionaleController
{
	public function indexAction()
	{
		$this->requiredPermission('access');

		return $this->render('GwAddonRisorseBundle:Colori:index.html.twig');
	}

	public function nuovoAction(Request $request)
	{
		$this->requiredPermission('nuovo_colore');

		$colore = new Colore();

		return $this->form($request, $colore, ColoreType::CREATE);
	}

	public function modificaAction($id, Request $request)
	{
		$this->requiredPermission('modifica_colore');

		$colore = $this
			->getDoctrine()
			->getRepository('GwAddonRisorseBundle:Colore')
			->findOneBy(array(
				'id'	=> $id,
			));

		if (!$colore)
		{
			return $this->redirectToRoute('gw_colori_index');
		}

		return $this->form($request, $colore, ColoreType::EDIT);
	}

	public function eliminaAction($id)
	{
		$this->requiredPermission('elimina_colore');

		$colore = $this
			->getDoctrine()
			->getRepository('GwAddonRisorseBundle:Colore')
			->findOneBy(array(
				'id'	=> $id,
			));

		if ($colore instanceof Colore)
		{
			$manager = $this
				->getDoctrine()
				->getManager();

			$this->logger->logUserAction(sprintf("ha eliminato il colore %s", $colore->__toString()));

			$manager->remove($colore);
			$manager->flush();
		}

		return $this->redirectToRoute('gw_colori_index');
	}

	public function listaAction()
	{
		$this->requiredPermission('gest_colori');

		$colori = $this
			->getDoctrine()
			->getRepository('GwAddonRisorseBundle:Colore')
			->findBy(array(), array(
				'descrizione'	=> 'ASC',
			));

		$_colori = array();

		foreach($colori as $colore)
		{
			$_colori[] = array(
				'id'				=> $colore->getId(),
				'codice'			=> $colore->getCodice(),
				'descrizione'		=> $colore->getDescrizione(),
			);
		}

		$response = new Response();
		$response->setContent(json_encode($_colori));
		$response->headers->set('Content-Type', 'application/json');

		return $response;
	}

	private function form(Request $request, Colore $colore, $type)
	{
		$form = $this->createForm(new ColoreType($this->get('c360.gwbase.licenza')), $colore);
		$form->handleRequest($request);

		if ($form->isValid())
		{
			$db = $this->getDoctrine();
			$manager = $db->getManager();

			if ($type == ColoreType::CREATE)
			{
				$this->logger->logUserAction(sprintf("ha creato il colore %s", $colore->__toString()));
			}
			else
			{
				$this->logger->logUserAction(sprintf("ha modificato il colore %s", $colore->__toString()));
			}

			$manager->persist($colore);
			$manager->flush();

			return $this->redirectToRoute('gw_colori_index');
		}

		return $this->render('GwAddonRisorseBundle:Colori:form.html.twig', array(
			'form'			=> $form->createView(),
		));
	}
}
