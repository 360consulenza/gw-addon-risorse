<?php

namespace Module\C360\GwAddonRisorse\Controller;

use GestionaleBundle\GestionaleController;
use Module\C360\GwAddonRisorse\Entity\GruppoTaglie;
use	Module\C360\GwAddonRisorse\Form\Type\GruppoTaglieType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class GruppiTaglieController extends GestionaleController
{
	public function indexAction()
	{
		$this->requiredPermission('access');

		return $this->render('GwAddonRisorseBundle:GruppiTaglie:index.html.twig');
	}

	public function nuovoAction(Request $request)
	{
		$this->requiredPermission('nuovo_gruppo_taglie');

		$gruppo_taglie = new GruppoTaglie();

		return $this->form($request, $gruppo_taglie, GruppoTaglieType::CREATE);
	}

	public function modificaAction($id, Request $request)
	{
		$this->requiredPermission('modifica_gruppo_taglie');

		$gruppo_taglie = $this
			->getDoctrine()
			->getRepository('GwAddonRisorseBundle:GruppoTaglie')
			->findOneBy(array(
				'id'	=> $id,
			));

		if (!$gruppo_taglie)
		{
			return $this->redirectToRoute('gw_gruppi_taglie_index');
		}

		return $this->form($request, $gruppo_taglie, GruppoTaglieType::EDIT);
	}

	public function eliminaAction($id)
	{
		$this->requiredPermission('elimina_gruppo_taglie');

		$gruppo_taglie = $this
			->getDoctrine()
			->getRepository('GwAddonRisorseBundle:GruppoTaglie')
			->findOneBy(array(
				'id'	=> $id,
			));

		if ($gruppo_taglie instanceof GruppoTaglie)
		{
			$manager = $this
				->getDoctrine()
				->getManager();

			$this->logger->logUserAction(sprintf("ha eliminato il gruppo_taglie %s", $gruppo_taglie->__toString()));

			$manager->remove($gruppo_taglie);
			$manager->flush();
		}

		return $this->redirectToRoute('gw_gruppi_taglie_index');
	}

	public function listaAction()
	{
		$this->requiredPermission('gest_gruppi_taglie');

		$gruppi_taglie = $this
			->getDoctrine()
			->getRepository('GwAddonRisorseBundle:GruppoTaglie')
			->findBy(array(), array(
				'descrizione'	=> 'ASC',
			));

		$_gruppi_taglie = array();

		foreach($gruppi_taglie as $gruppo_taglie)
		{
			$_gruppi_taglie[] = array(
				'id'				=> $gruppo_taglie->getId(),
				'codice'			=> $gruppo_taglie->getCodice(),
				'descrizione'		=> $gruppo_taglie->getDescrizione(),
			);
		}

		$response = new Response();
		$response->setContent(json_encode($_gruppi_taglie));
		$response->headers->set('Content-Type', 'application/json');

		return $response;
	}

	private function form(Request $request, GruppoTaglie $gruppo_taglie, $type)
	{
		$form = $this->createForm(new GruppoTaglieType($this->get('c360.gwbase.licenza')), $gruppo_taglie);
		$form->handleRequest($request);

		if ($form->isValid())
		{
			$db = $this->getDoctrine();
			$manager = $db->getManager();

			if ($type == GruppoTaglieType::CREATE)
			{
				$this->logger->logUserAction(sprintf("ha creato il gruppo_taglie %s", $gruppo_taglie->__toString()));
			}
			else
			{
				$this->logger->logUserAction(sprintf("ha modificato il gruppo_taglie %s", $gruppo_taglie->__toString()));
			}

			$manager->persist($gruppo_taglie);
			$manager->flush();

			return $this->redirectToRoute('gw_gruppi_taglie_index');
		}

		return $this->render('GwAddonRisorseBundle:GruppiTaglie:form.html.twig', array(
			'form'			=> $form->createView(),
		));
	}
}
