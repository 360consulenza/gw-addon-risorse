<?php

namespace Module\C360\GwAddonRisorse\Controller;

use GestionaleBundle\GestionaleController;
//use Module\C360\GwBase\Entity\Agente;
//use Module\C360\GwBase\Form\Type\AgenteType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class StampaArticoliController extends GestionaleController
{
	public function indexAction()
	{
		$this->requiredPermission('access');

		return $this->render('GwAddonRisorseBundle:StampaArticoli:index.html.twig');
	}
}
