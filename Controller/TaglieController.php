<?php

namespace Module\C360\GwAddonRisorse\Controller;

use GestionaleBundle\GestionaleController;
use Module\C360\GwAddonRisorse\Entity\Taglia;
use	Module\C360\GwAddonRisorse\Form\Type\TagliaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TaglieController extends GestionaleController
{
	public function indexAction()
	{
		$this->requiredPermission('access');

		return $this->render('GwAddonRisorseBundle:Taglie:index.html.twig');
	}

	public function nuovoAction(Request $request)
	{
		$this->requiredPermission('gest_taglie');

		$taglia = new Taglia();

		return $this->form($request, $taglia, TagliaType::CREATE);
	}

	public function modificaAction($id, Request $request)
	{
		$this->requiredPermission('gest_taglie');

		$taglia = $this
			->getDoctrine()
			->getRepository('GwAddonRisorseBundle:Taglia')
			->findOneBy(array(
				'id'	=> $id,
			));

		if (!$taglia)
		{
			return $this->redirectToRoute('gw_taglie_index');
		}

		return $this->form($request, $taglia, TagliaType::EDIT);
	}

	public function eliminaAction($id)
	{
		$this->requiredPermission('gest_taglie');

		$taglia = $this
			->getDoctrine()
			->getRepository('GwAddonRisorseBundle:Taglia')
			->findOneBy(array(
				'id'	=> $id,
			));

		if ($taglia instanceof Taglia)
		{
			$manager = $this
				->getDoctrine()
				->getManager();

			$this->logger->logUserAction(sprintf("ha eliminato la taglia %s relativa al gruppo di taglie %s", $taglia->__toString(), $taglia->getGruppoTaglie()->__toString()));

			$manager->remove($taglia);
			$manager->flush();
		}

		return $this->redirectToRoute('gw_taglie_index');
	}

	public function listaAction()
	{
		$this->requiredPermission('gest_taglie');

		$taglie = $this
			->getDoctrine()
			->getRepository('GwAddonRisorseBundle:Taglia')
			->findBy(array(), array(
				'codice'	=> 'ASC',
			));

		$_taglie = array();

		foreach($taglie as $taglia)
		{
			$_taglie[] = array(
				'id'				=> $taglia->getId(),
				'gruppo_taglie'	=> $taglia->getGruppoTaglie()->__toString(),
				'codice'				=> $taglia->getCodice(),
				'descrizione'		=> $taglia->getDescrizione(),
			);
		}

		$response = new Response();
		$response->setContent(json_encode($_taglie));
		$response->headers->set('Content-Type', 'application/json');

		return $response;
	}

	private function form(Request $request, Taglia $taglia, $type)
	{
		$form = $this->createForm(new TagliaType($this->get('c360.gwbase.licenza')), $taglia);
		$form->handleRequest($request);

		if ($form->isValid())
		{
			$db = $this->getDoctrine();
			$manager = $db->getManager();

			if ($type == TagliaType::CREATE)
			{
				$this->logger->logUserAction(sprintf("ha creato la taglia %s relativa alla gruppo di taglie %s", $taglia->__toString(), $taglia->getGruppoTaglie()->__toString()));
			}
			else
			{
				$this->logger->logUserAction(sprintf("ha modificato la taglia %s relativa alla gruppo di taglie %s", $taglia->__toString(), $taglia->getGruppoTaglie()->__toString()));
			}

			$manager->persist($taglia);
			$manager->flush();

			return $this->redirectToRoute('gw_taglie_index');
		}

		return $this->render('GwAddonRisorseBundle:Taglie:form.html.twig', array(
			'form'			=> $form->createView(),
		));
	}
}
